# Supplementary scripts for Li *et al* 2018 Correspondance (Nature Communications, submitted)

This repository contains a collection of scripts that will reproduce the results described in Vaquero, Norton, and Barash 2018 (submitted).  It contains a configuration file `setup.sh` and an executable script `command.sh`, as well as two run specification files `figA_time.tsv` and `figB_iir.tsv`.  Tool-specific scripts can be found in their respective directories.

## Setup

You will need the following software packages:

- [rMATS v4.0.2, released May 2018](http://rnaseq-mats.sourceforge.net/rmats4.0.2/)
- [LeafCutter v0.2.7, released September 2017](https://github.com/davidaknowles/leafcutter/releases/tag/v0.2.7)
- [MAJIQ v1.1.3a, released May 2018](https://majiq.biociphers.org)
    - NOTE: For MAJIQ runtime and memory benchmarking, we also evaluated versions 1.0.8, released April 2017, and 2.0.0, unreleased (manuscript in preparation).
    These same scripts should work just fine for version 2.0.0, but not for the legacy 1.0.8 version.  Scripts for MAJIQ 1.0.8 are not provided as this software version is deprecated.

You will also need to clone the [shared validation scripts](/biociphers/validations_tools), and prepare RNA-seq alignments, mapped to the `hg19` reference genome, from specific GTEx SRAs which have been made publicly available on dbGAP.  The required SRR IDs are listed in the included `srr_ids.txt`.

Edit `setup.sh` with the directories to which you downloaded the above tools, repositories, and processed BAM files.

## Running

In your UNIX command shell, type `sh command.sh`.  If you get any "file not found" errors, make sure you have your paths set up correctly in `setup.sh` and try again.  Depending on when the crash occurs, you may edit `command.sh` to comment any execution blocks you do not want to rerun.

## Output

The script will output copies of all the figures in `.pdf` format to the current working directory.

