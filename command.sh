#!/bin/bash

. setup.sh

#GENERATE JUNC FILES FOR LEAFCUTTER
sh $PROJECTDIR/leafcutter/gen_junc.sh




#FIGURE A: TIME
#PREPARE THE RUNS
python $VALIDATION_TOOLS/gen_tool_runs.py leafcutter -i $PROJECTDIR/figA_time.tsv -b $PROJECTDIR/leafcutter/juncfiles -o $PROJECTDIR/leafcutter/ -j 16 -db $LEAFCUTTERDIR/leafcutter/data/gencode19_exons.txt.gz --bindir $LEAFCUTTERDIR timing
python $VALIDATION_TOOLS/gen_tool_runs.py rmats -i $PROJECTDIR/figA_time.tsv -b $BAMFILES/ -o $PROJECTDIR/rmats/ -j 16 -db ./ensembl.hg19.gtf --readlen 150 --threshold 0.2 --bindir $RMATSDIR timing
python $VALIDATION_TOOLS/gen_tool_runs.py majiq -i $PROJECTDIR/figA_time.tsv -b $BAMFILES/ -o $PROJECTDIR/majiq/ -j 16 -db ./ensembl.hg19.gff3 --build-flags --separate-builder --readlen 250 --bindir $MAJIQDIR timing

#MAJIQ

cd leafcutter
sh run_timing.sh
cd ..

cd rmats
sh run_timing.sh
cd ..

cd majiq
sh run_timing.sh
cd ..

#MAJIQ

#FIGURE B: IIR
#PREPARE THE RUNS
python $VALIDATION_TOOLS/gen_tool_runs.py leafcutter -i $PROJECTDIR/figB_iir.tsv -b $PROJECTDIR/leafcutter/juncfiles -o $PROJECTDIR/leafcutter/ -j 16 -db $LEAFCUTTERDIR/leafcutter/data/gencode19_exons.txt.gz --bindir $LEAFCUTTERDIR iir_subsamples
python $VALIDATION_TOOLS/gen_tool_runs.py rmats -i $PROJECTDIR/figB_iir.tsv -b $BAMFILES/ -o $PROJECTDIR/rmats/ -j 16 -db ./ensembl.hg19.gtf --readlen 150 --threshold 0.2 --bindir $RMATSDIR iir_subsamples
python $VALIDATION_TOOLS/gen_tool_runs.py majiq -i $PROJECTDIR/figB_iir.tsv -b $BAMFILES/ -o $PROJECTDIR/majiq/ -j 16 -db ./ensembl.hg19.gff3 --build-flags --readlen 250 --bindir $MAJIQDIR --rr-pattern Muscle_b_Cerebellum_b iir_subsamples
#MAJIQ

cd leafcutter
sh run_iir_subsamples.sh
sh RR.leafcutter_iir_subset.sh
cd ..

cd rmats
sh run_iir_subsamples.sh
sh RR.rmats_iir_subset.sh
cd ..


cd majiq
sh run_iir_subsamples.sh
sh RR.majiq_iir_subset.sh
cd ..


echo "" > majiq.all_iir.log
echo "" > leafcutter.all_iir.log
echo "" > leafcutter.onlypval.all_iir.log
echo "" > rmats.all_iir.log

for nsamples in 3 4 5 6 7 8 9 10
do
    for replicate in 1 2 3
    do
        condition=${nsamples}_vs_${nsamples}_rep_${replicate}
        cat majiq/RR/logs/Cerebellum_${condition}.log majiq/RR/logs/Muscle_${condition}.log >> majiq.all_iir.log
        cat rmats/RR/logs/Cerebellum_${condition}.log majiq/RR/logs/Muscle_${condition}.log >> rmats.all_iir.log
        cat leafcutter/RR/logs/Cerebellum_${condition}.log majiq/RR/logs/Muscle_${condition}.log >> leafcutter.all_iir.log
        cat leafcutter/RR/logs/Cerebellum_${condition}.log majiq/RR/logs/Muscle_${condition}.onlypval.log >> leafcutter.onlypval.all_iir.log
    done
done

python3.6 plot_iir_from_logs.py


#FIGURE E: RR

python $VALIDATION_TOOLS/plot_rr.py -c majiq Majiq majiq/RR/Signal_5_vs_5_1/majiq.ratios.pickle  -c leafcutter Leafcutter_onlypval leafcutter/RR/Signal_5_vs_5_1_onlypval/leafcutter.ratios.pickle -c leafcutter Leafcutter leafcutter/RR/Signal_5_vs_5_1/leafcutter.ratios.pickle -c rmats RMATS rmats/RR/Signal_5_vs_5_1/rmats.ratios.pickle  -o ./figE.pdf -m 3200


#FIGURE F: RTPCR



