import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from collections import defaultdict
import os
import typing
import brewer2mpl
import re

cmap = np.array(brewer2mpl.get_map('Paired', 'qualitative', 11).mpl_colors)

labels = {
    'deltapsi': ('MAJIQ', cmap[1]),
    'deltapsi.balanced': ('MAJIQ', cmap[0]),
    'ttest': ('T Test', cmap[2]),
    'tnom': ('TNOM', cmap[8]),
    'infoscore': ('InfoScore', cmap[4]),
    'wilcoxon': ('Wilcoxon', cmap[5]),
    'rmats': ('rMATS', cmap[3]),
    'leafcutter.onlypval': ('LeafCutter', cmap[6]),
    'leafcutter': ('LeafCutter w. $|\Delta\Psi| \geq 20\%$', cmap[7]),
}
x_vals = range(3, 11)
stats = 'P(|dPSI|>=0.20) per LSV junction', 'ttest', 'tnom', 'infoscore', 'wilcoxon'
tissues = 'Muscle', 'Cerebellum'
matplotlib.rc('font', size=16)


class Dataset(defaultdict):
    def __init__(self, dtype: typing.Type, ident: str, ylabel: str):
        super().__init__(dtype)
        self.ylabel = ylabel
        self.ident = ident


class IIRDataset(Dataset):
    def __init__(self, ident: str, ylabel: str):
        super().__init__(lambda: defaultdict(list), ident, ylabel)

    def reshape(self, shape: typing.Iterable):
        for stat, tissue_dict in self.items():
            for tissue, iir in tissue_dict.items():
                tissue_dict[tissue] = np.array(iir).reshape(shape)

    def append(self, tissue: str, stat: str, value: typing.Any):
        self[stat][tissue].append(value)

    def transpose(self):
        for stat, tissue_dict in self.items():
            for tissue, iir in tissue_dict.items():
                tissue_dict[tissue] = np.transpose(iir)


class RRDataset(Dataset):
    def __init__(self, ident: str, ylabel: str):
        super().__init__(list, ident, ylabel)

    def reshape(self, shape: typing.Iterable):
        for stat, rr in self.items():
            self[stat] = np.array(rr).reshape(shape)

    def append(self, stat: str, value: typing.Any):
        self[stat].append(value)

    def transpose(self):
        for stat, rr in self.items():
            self[stat] = np.transpose(rr)


class DatasetCollection(list):
    def update(self, other: typing.Any):
        if not isinstance(other, DatasetCollection):
            raise NotImplementedError
        for x, y in zip(self, other):
            x.update(y)

    def pop_stat(self, stat):
        for x in self:
            x.pop(stat, None)

    def reshape(self, shape):
        for x in self:
            x.reshape(shape)
        return self

    def transpose(self):
        for x in self:
            x.transpose()
        return self

    def append(self, *values):
        if isinstance(self[0], IIRDataset):
            tissue, stat, *rest = values
            argt = (tissue, stat)
        else:
            stat, *rest = values
            argt = (stat,)
        for dataset, value in zip(self, rest):
            dataset.append(*(argt + (value,)))


def find(iterable, predicate):
    for x in iterable:
        if predicate(x):
            return x


def read_majiq_rr(
        fp: str,
        *args,
        **kwargs
) -> typing.Generator[typing.Tuple[str, int, float], None, None]:
    stat = None
    n_a = -1
    rr_a = -1.0
    for line in fp:
        line = line.strip()
        if line.startswith('Calculating RR for '):
            stat = find(stats, line.__contains__)
            if stat == stats[0]:
                stat = 'deltapsi'
        elif line.startswith('Number of significant events at alpha=0.05: '):
            if n_a < 0:
                n_a = int(line.split()[-1])
        elif line.startswith('Reproducibility ratio: '):
            rr_a = float(line.split()[-1])
            yield stat, n_a, rr_a
            stat = None
            n_a = -1
            rr_a = -1.0


def read_majiq_iir(
        fp: str,
        tissues: typing.Iterable[str] = tissues,
        method: str = 'deltapsi',
        *args,
        **kwargs
) -> typing.Generator[typing.Tuple[str, str, float, int, int], None, None]:
    for line in fp:
        if 'N NoSignal' in line:
            split = line.split(' = ')
            nnosig = int(split[1].split(',')[0])
            nsig = int(split[2].split(',')[0])
            iir = float(split[3].strip('\n%'))
            tissue = find(tissues, line.__contains__)
            stat = find(stats, line.__contains__)
            if stat == stats[0]:
                stat = method
            yield tissue, stat, iir, nnosig, nsig


def read_other_rr(
        fp: str,
        method: str = 'rmats',
        x_vals: typing.Iterable[float] = x_vals
) -> typing.Generator[typing.Tuple[str, int, float], None, None]:
    ns = defaultdict(lambda: ([], []))
    k = -1
    is_nosignal = True
    for line in fp:
        line = line.strip()
        if line.endswith('.log'):
            is_nosignal = 'NoSignal' in line
            kind, new_k, *_ = line.split('_')
            new_k = int(new_k.split('vs')[0])
            if new_k != k:
                k = new_k
        elif 'RR=' in line and not is_nosignal:
            _, neqline, rreqline = line.split()
            n_a = int(neqline.strip(',').split('=')[1])
            rr_a = float(rreqline.split('=')[1])
            ns[k][0].append(n_a)
            ns[k][1].append(rr_a)
    for k in x_vals:
        for n_a, rr_a in zip(*ns[k]):
            yield method, n_a, rr_a


def read_other_iir(
        fp: str,
        method: str = 'rmats',
        tissues: typing.Tuple[str] = tissues,
        x_vals: typing.Iterable[float] = x_vals
) -> typing.Generator[typing.Tuple[str, str, float, int, int], None, None]:
    ns = [{tiss: defaultdict(list) for tiss in tissues} for i in range(2)]
    k = -1
    phase = 0
    for line in fp:
        line = line.strip()
        if line.endswith('.log'):
            kind, new_k, *_ = line.split('_')
            new_k = int(new_k.split('vs')[0])
            if new_k != k:
                k = new_k
                phase = 0
        elif ' and ' in line:
            ns[(phase % 4) // 2][tissues[phase % 2]][k].append(int(line.split()[-1]))
            phase += 1
    for k in x_vals:
        for tiss in tissues:
            nnosig = ns[0][tiss][k]
            nsig = ns[1][tiss][k]
            for n1, n2 in zip(nnosig, nsig):
                iir = n1 / n2 if n2 else 0.0
                yield tiss, method, iir * 100, n1, n2


def read_other_iir_2(
        fp: str,
        method: str = 'rmats',
        tissues: typing.Tuple[str] = tissues,
        x_vals: typing.Iterable[float] = x_vals
) -> typing.Generator[typing.Tuple[str, str, float, int, int], None, None]:
    tiss_idx = 0
    for line in fp:
        match = re.search(f'N_nosignal = (\d+), N_signal = (\d+), IIR = (0\.\d+)', line)
        if match is not None:
            n_nosignal, n_signal, iir = match.groups()
            n_nosignal = int(n_nosignal)
            n_signal = int(n_signal)
            iir = float(iir)
            yield tissues[tiss_idx], method, iir * 100, n_nosignal, n_signal
            tiss_idx ^= 1


def read_file(
        fname: str,
        dset_cls: typing.Type = Dataset,
        dataset_initargs: typing.Iterable = (),
        shape: typing.Iterable = (3, -1),
        read_fn: typing.Callable = read_majiq_iir,
        read_args: typing.Iterable = (),
        read_kw: typing.Mapping = {},
        transpose: bool = False
) -> DatasetCollection:
    coll = DatasetCollection([dset_cls(*args) for args in dataset_initargs])

    with open(fname) as fp:
        for values in read_fn(fp, *read_args, **read_kw):
            coll.append(*values)

    coll.reshape(shape)
    if transpose:
        coll.transpose()
    return coll


def read_iir(
        fname: str,
        read_fn: typing.Callable[
            [str, str, typing.Iterable[str], typing.Iterable[float]],
            typing.Generator[typing.Tuple[str, str, float, int, int], None, None]
        ] = read_majiq_iir,
        **kwargs
) -> DatasetCollection:
    dataset_initargs = [
        ('iir', 'IIR (percent)'),
        ('nnosig', 'N NoSignal'),
        ('nsig', 'N Signal')
    ]
    return read_file(fname, read_fn=read_fn, dset_cls=IIRDataset, dataset_initargs=dataset_initargs, **kwargs)


def read_rr(
        fname: str,
        read_fn: typing.Callable[
            [str, str, typing.Iterable[float]],
            typing.Generator[typing.Tuple[str, int, float], None, None]
        ] = read_majiq_rr,
        **kwargs
) -> DatasetCollection:
    dataset_initargs = [
        ('n_a', 'Number of significantly changing events'),
        ('rr', 'Reproducibility Ratio')
    ]
    return read_file(fname, read_fn=read_fn, dset_cls=RRDataset, dataset_initargs=dataset_initargs, **kwargs)


def plot_dict_single(
        xbins: np.ndarray,
        ydict: Dataset,
        fname: str,
        *skiptests: str,
        tissue: str = tissues[0],
        which_x: typing.Container[int] = None,
        replicates_as_separate: bool = False,
        title: str = None,
        rescale_y: float = None
):
    title = title or tissue
    if which_x:
        x_idx, = np.where([xx in which_x for xx in x_vals])
    else:
        x_idx = range(len(x_vals))
    f1 = plt.figure(figsize=[10, 10])
    _labels = {test: params for test, params in labels.items() if test not in skiptests and test in ydict}
    fullwidth = xbins[1] - xbins[0] if len(xbins) >= 2 else 1
    width = 0.9 * fullwidth / len(_labels)
    for (test, (name, color)), xoffset in zip(_labels.items(), np.arange(0.05 * fullwidth, fullwidth, width)):
        if isinstance(ydict, IIRDataset):
            yvals = ydict[test][tissue]
        else:
            yvals = ydict[test]
        if replicates_as_separate:
            nreps = yvals.shape[0]
            d_color = (np.ones(3) - color) / (2 * nreps)
            for ii, yvec in enumerate(yvals):
                plt.bar(
                    xbins[x_idx] + xoffset + ii * width / nreps,
                    yvec[x_idx],
                    facecolor=color + ii * d_color,
                    label=f'{name} (replicate {ii})',
                    capsize=2,
                    width=width / nreps,
                    edgecolor='k',
                    align='edge'
                )
        else:
            mu = np.mean(yvals, axis=0)
            sd = np.std(yvals, axis=0)
            plt.bar(
                xbins[x_idx] + xoffset,
                mu[x_idx],
                yerr=sd[x_idx],
                facecolor=color,
                label=f'{name}',
                capsize=2,
                width=width,
                edgecolor='k',
                align='edge'
            )
    plt.legend(loc=0, fontsize='large')
    plt.xlabel('Number of patients in GTEx')
    plt.ylabel(ydict.ylabel)
    # plt.title(tissue)
    if ydict.ylabel == 'IIR (percent)' and rescale_y is not None:
        ymin, ymax = plt.ylim()
        scale = rescale_y / ymax
        plt.ylim(ymin * scale, rescale_y)
    xticks = xbins[x_idx] + 0.5 * fullwidth
    plt.xticks(xticks)
    plt.xlim(xbins[x_idx[0]] - 0.5 * fullwidth, xbins[x_idx[-1]] + 1.5 * fullwidth)
    plt.savefig(fname, transparent=True)
    plt.close()


def plot_single(
        datasets: DatasetCollection,
        *,
        x_vals: typing.Iterable[float] = x_vals,
        directory: str = '.',
        skiptests: typing.Iterable[str] = (),
        condition: str = '',
        tissues: typing.Iterable[str] = tissues,
        which_x: typing.Container[int] = None,
        replicates_as_separate: bool = False,
        title: str = None,
        rescale_y: float = None,
        fname_override: str = None
):
    os.makedirs(directory, exist_ok=True)
    xbins = np.array(x_vals, dtype=float)
    if len(xbins) >= 2:
        xbins -= 0.5 * (xbins[1] - xbins[0])
    else:
        xbins -= 0.5
    for dataset in datasets:
        if not isinstance(dataset, IIRDataset):
            tissues = '',
        for tissue in tissues:
            underscore = '_' * bool(tissue)
            fname = fname_override or f'{directory}/{dataset.ident}{condition}{underscore}{tissue.lower()}.pdf'
            plot_dict_single(
                xbins,
                dataset,
                fname,
                *skiptests,
                tissue=tissue,
                which_x=which_x,
                replicates_as_separate=replicates_as_separate,
                title=title,
                rescale_y=rescale_y
            )


def main():
    data = read_iir('majiq.all_iir.log', transpose=True, shape=(-1, 3))
    data.update(read_iir('leafcutter.all_iir.log', read_fn=read_other_iir_2,
                         read_kw={'method': 'leafcutter', 'tissues': (tissues[1], tissues[0])}, transpose=True, shape=(-1, 3)))
    data.update(read_iir('leafcutter.onlypval.all_iir.log', read_fn=read_other_iir_2,
                         read_kw={'method': 'leafcutter.onlypval', 'tissues': (tissues[1], tissues[0])}, transpose=True, shape=(-1, 3)))
    data.update(read_iir('rmats.all_iir.log', read_fn=read_other_iir_2,
                         read_kw={'method': 'rmats'}, transpose=True, shape=(-1, 3)))
    plot_single(data, skiptests=('ttest', 'tnom', 'wilcoxon', 'infoscore'),
                fname_override='figB.pdf', which_x=range(3, 8))


if __name__ == '__main__':
    main()
