#!/bin/bash
mkdir -p RR/logs

python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_3_vs_3_1_Muscle_a_Muscle_b output_ds/iir_subset_3_vs_3_1_Muscle_a_Cerebellum_a -m Muscle_iir_3_vs_3_rep_1  > RR/Muscle_3_vs_3_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_4_vs_4_1_Muscle_a_Muscle_b output_ds/iir_subset_4_vs_4_1_Muscle_a_Cerebellum_a -m Muscle_iir_4_vs_4_rep_1  > RR/Muscle_4_vs_4_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_5_vs_5_1_Muscle_a_Muscle_b output_ds/iir_subset_5_vs_5_1_Muscle_a_Cerebellum_a -m Muscle_iir_5_vs_5_rep_1  > RR/Muscle_5_vs_5_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_6_vs_6_1_Muscle_a_Muscle_b output_ds/iir_subset_6_vs_6_1_Muscle_a_Cerebellum_a -m Muscle_iir_6_vs_6_rep_1  > RR/Muscle_6_vs_6_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_7_vs_7_1_Muscle_a_Muscle_b output_ds/iir_subset_7_vs_7_1_Muscle_a_Cerebellum_a -m Muscle_iir_7_vs_7_rep_1  > RR/Muscle_7_vs_7_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_8_vs_8_1_Muscle_a_Muscle_b output_ds/iir_subset_8_vs_8_1_Muscle_a_Cerebellum_a -m Muscle_iir_8_vs_8_rep_1  > RR/Muscle_8_vs_8_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_9_vs_9_1_Muscle_a_Muscle_b output_ds/iir_subset_9_vs_9_1_Muscle_a_Cerebellum_a -m Muscle_iir_9_vs_9_rep_1  > RR/Muscle_9_vs_9_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_10_vs_10_1_Muscle_a_Muscle_b output_ds/iir_subset_10_vs_10_1_Muscle_a_Cerebellum_a -m Muscle_iir_10_vs_10_rep_1  > RR/Muscle_10_vs_10_rep_1.log

python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_3_vs_3_2_Muscle_a_Muscle_b output_ds/iir_subset_3_vs_3_2_Muscle_a_Cerebellum_a -m Muscle_iir_3_vs_3_rep_2  > RR/Muscle_3_vs_3_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_4_vs_4_2_Muscle_a_Muscle_b output_ds/iir_subset_4_vs_4_2_Muscle_a_Cerebellum_a -m Muscle_iir_4_vs_4_rep_2  > RR/Muscle_4_vs_4_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_5_vs_5_2_Muscle_a_Muscle_b output_ds/iir_subset_5_vs_5_2_Muscle_a_Cerebellum_a -m Muscle_iir_5_vs_5_rep_2  > RR/Muscle_5_vs_5_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_6_vs_6_2_Muscle_a_Muscle_b output_ds/iir_subset_6_vs_6_2_Muscle_a_Cerebellum_a -m Muscle_iir_6_vs_6_rep_2  > RR/Muscle_6_vs_6_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_7_vs_7_2_Muscle_a_Muscle_b output_ds/iir_subset_7_vs_7_2_Muscle_a_Cerebellum_a -m Muscle_iir_7_vs_7_rep_2  > RR/Muscle_7_vs_7_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_8_vs_8_2_Muscle_a_Muscle_b output_ds/iir_subset_8_vs_8_2_Muscle_a_Cerebellum_a -m Muscle_iir_8_vs_8_rep_2  > RR/Muscle_8_vs_8_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_9_vs_9_2_Muscle_a_Muscle_b output_ds/iir_subset_9_vs_9_2_Muscle_a_Cerebellum_a -m Muscle_iir_9_vs_9_rep_2  > RR/Muscle_9_vs_9_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_10_vs_10_2_Muscle_a_Muscle_b output_ds/iir_subset_10_vs_10_2_Muscle_a_Cerebellum_a -m Muscle_iir_10_vs_10_rep_2  > RR/Muscle_10_vs_10_rep_2.log

python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_3_vs_3_3_Muscle_a_Muscle_b output_ds/iir_subset_3_vs_3_3_Muscle_a_Cerebellum_a -m Muscle_iir_3_vs_3_rep_3  > RR/Muscle_3_vs_3_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_4_vs_4_3_Muscle_a_Muscle_b output_ds/iir_subset_4_vs_4_3_Muscle_a_Cerebellum_a -m Muscle_iir_4_vs_4_rep_3  > RR/Muscle_4_vs_4_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_5_vs_5_3_Muscle_a_Muscle_b output_ds/iir_subset_5_vs_5_3_Muscle_a_Cerebellum_a -m Muscle_iir_5_vs_5_rep_3  > RR/Muscle_5_vs_5_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_6_vs_6_3_Muscle_a_Muscle_b output_ds/iir_subset_6_vs_6_3_Muscle_a_Cerebellum_a -m Muscle_iir_6_vs_6_rep_3  > RR/Muscle_6_vs_6_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_7_vs_7_3_Muscle_a_Muscle_b output_ds/iir_subset_7_vs_7_3_Muscle_a_Cerebellum_a -m Muscle_iir_7_vs_7_rep_3  > RR/Muscle_7_vs_7_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_8_vs_8_3_Muscle_a_Muscle_b output_ds/iir_subset_8_vs_8_3_Muscle_a_Cerebellum_a -m Muscle_iir_8_vs_8_rep_3  > RR/Muscle_8_vs_8_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_9_vs_9_3_Muscle_a_Muscle_b output_ds/iir_subset_9_vs_9_3_Muscle_a_Cerebellum_a -m Muscle_iir_9_vs_9_rep_3  > RR/Muscle_9_vs_9_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_10_vs_10_3_Muscle_a_Muscle_b output_ds/iir_subset_10_vs_10_3_Muscle_a_Cerebellum_a -m Muscle_iir_10_vs_10_rep_3  > RR/Muscle_10_vs_10_rep_3.log


python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_3_vs_3_1_Cerebellum_a_Cerebellum_b output_ds/iir_subset_3_vs_3_1_Muscle_b_Cerebellum_b -m Cerebellum_3_vs_3_rep_1  > RR/Cerebellum_3_vs_3_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_4_vs_4_1_Cerebellum_a_Cerebellum_b output_ds/iir_subset_4_vs_4_1_Muscle_b_Cerebellum_b -m Cerebellum_4_vs_4_rep_1  > RR/Cerebellum_4_vs_4_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_5_vs_5_1_Cerebellum_a_Cerebellum_b output_ds/iir_subset_5_vs_5_1_Muscle_b_Cerebellum_b -m Cerebellum_5_vs_5_rep_1  > RR/Cerebellum_5_vs_5_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_6_vs_6_1_Cerebellum_a_Cerebellum_b output_ds/iir_subset_6_vs_6_1_Muscle_b_Cerebellum_b -m Cerebellum_6_vs_6_rep_1  > RR/Cerebellum_6_vs_6_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_7_vs_7_1_Cerebellum_a_Cerebellum_b output_ds/iir_subset_7_vs_7_1_Muscle_b_Cerebellum_b -m Cerebellum_7_vs_7_rep_1  > RR/Cerebellum_7_vs_7_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_8_vs_8_1_Cerebellum_a_Cerebellum_b output_ds/iir_subset_8_vs_8_1_Muscle_b_Cerebellum_b -m Cerebellum_8_vs_8_rep_1  > RR/Cerebellum_8_vs_8_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_9_vs_9_1_Cerebellum_a_Cerebellum_b output_ds/iir_subset_9_vs_9_1_Muscle_b_Cerebellum_b -m Cerebellum_9_vs_9_rep_1  > RR/Cerebellum_9_vs_9_rep_1.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_10_vs_10_1_Cerebellum_a_Cerebellum_b output_ds/iir_subset_10_vs_10_1_Muscle_b_Cerebellum_b -m Cerebellum_10_vs_10_rep_1  > RR/Cerebellum_10_vs_10_rep_1.log

python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_3_vs_3_2_Cerebellum_a_Cerebellum_b output_ds/iir_subset_3_vs_3_2_Muscle_b_Cerebellum_b -m Cerebellum_3_vs_3_rep_2  > RR/Cerebellum_3_vs_3_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_4_vs_4_2_Cerebellum_a_Cerebellum_b output_ds/iir_subset_4_vs_4_2_Muscle_b_Cerebellum_b -m Cerebellum_4_vs_4_rep_2  > RR/Cerebellum_4_vs_4_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_5_vs_5_2_Cerebellum_a_Cerebellum_b output_ds/iir_subset_5_vs_5_2_Muscle_b_Cerebellum_b -m Cerebellum_5_vs_5_rep_2  > RR/Cerebellum_5_vs_5_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_6_vs_6_2_Cerebellum_a_Cerebellum_b output_ds/iir_subset_6_vs_6_2_Muscle_b_Cerebellum_b -m Cerebellum_6_vs_6_rep_2  > RR/Cerebellum_6_vs_6_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_7_vs_7_2_Cerebellum_a_Cerebellum_b output_ds/iir_subset_7_vs_7_2_Muscle_b_Cerebellum_b -m Cerebellum_7_vs_7_rep_2  > RR/Cerebellum_7_vs_7_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_8_vs_8_2_Cerebellum_a_Cerebellum_b output_ds/iir_subset_8_vs_8_2_Muscle_b_Cerebellum_b -m Cerebellum_8_vs_8_rep_2  > RR/Cerebellum_8_vs_8_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_9_vs_9_2_Cerebellum_a_Cerebellum_b output_ds/iir_subset_9_vs_9_2_Muscle_b_Cerebellum_b -m Cerebellum_9_vs_9_rep_2  > RR/Cerebellum_9_vs_9_rep_2.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_10_vs_10_2_Cerebellum_a_Cerebellum_b output_ds/iir_subset_10_vs_10_2_Muscle_b_Cerebellum_b -m Cerebellum_10_vs_10_rep_2  > RR/Cerebellum_10_vs_10_rep_2.log

python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_3_vs_3_3_Cerebellum_a_Cerebellum_b output_ds/iir_subset_3_vs_3_3_Muscle_b_Cerebellum_b -m Cerebellum_3_vs_3_rep_3  > RR/Cerebellum_3_vs_3_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_4_vs_4_3_Cerebellum_a_Cerebellum_b output_ds/iir_subset_4_vs_4_3_Muscle_b_Cerebellum_b -m Cerebellum_4_vs_4_rep_3  > RR/Cerebellum_4_vs_4_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_5_vs_5_3_Cerebellum_a_Cerebellum_b output_ds/iir_subset_5_vs_5_3_Muscle_b_Cerebellum_b -m Cerebellum_5_vs_5_rep_3  > RR/Cerebellum_5_vs_5_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_6_vs_6_3_Cerebellum_a_Cerebellum_b output_ds/iir_subset_6_vs_6_3_Muscle_b_Cerebellum_b -m Cerebellum_6_vs_6_rep_3  > RR/Cerebellum_6_vs_6_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_7_vs_7_3_Cerebellum_a_Cerebellum_b output_ds/iir_subset_7_vs_7_3_Muscle_b_Cerebellum_b -m Cerebellum_7_vs_7_rep_3  > RR/Cerebellum_7_vs_7_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_8_vs_8_3_Cerebellum_a_Cerebellum_b output_ds/iir_subset_8_vs_8_3_Muscle_b_Cerebellum_b -m Cerebellum_8_vs_8_rep_3  > RR/Cerebellum_8_vs_8_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_9_vs_9_3_Cerebellum_a_Cerebellum_b output_ds/iir_subset_9_vs_9_3_Muscle_b_Cerebellum_b -m Cerebellum_9_vs_9_rep_3  > RR/Cerebellum_9_vs_9_rep_3.log
python $VALIDATION_TOOLS/run_iir.py -t rmats output_ds/iir_subset_10_vs_10_3_Cerebellum_a_Cerebellum_b output_ds/iir_subset_10_vs_10_3_Muscle_b_Cerebellum_b -m Cerebellum_10_vs_10_rep_3  > RR/Cerebellum_10_vs_10_rep_3.log


python $VALIDATION_TOOLS/run_repro.py -t rmats output_ds/iir_subset_5_vs_5_1_Muscle_a_Cerebellum_a output_ds/iir_subset_5_vs_5_1_Muscle_b_Cerebellum_b --s_pval -o RR/Signal_5_vs_5_1_spval  > RR/logs/Signal_5_vs_5_1_spval.log                                                                                                                                                                                       
python $VALIDATION_TOOLS/run_repro.py -t rmats output_ds/iir_subset_5_vs_5_1_Muscle_a_Cerebellum_a output_ds/iir_subset_5_vs_5_1_Muscle_b_Cerebellum_b -o RR/Signal_5_vs_5_1  > RR/logs/Signal_5_vs_5_1.log 
